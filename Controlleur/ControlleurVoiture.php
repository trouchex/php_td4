<?php
require_once '../Modele/ModeleVoiture.php';
class ControleurVoiture {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur



    public static function afficherListe() : void {
        //appel au modèle pour gerer la BD
        $chemin = 'voiture/liste.php';
        $voitures =  ModeleVoiture::getVoitures();
        /* */
        self::afficherVue($chemin, ["voitures" => $voitures]);//"redirige" vers la vue
    }
    public static function afficherDetail() : void {
        $immatriculation = $_GET['Immatriculation'];
        $chemin = 'voiture/detail.php';
        $voiture = ModeleVoiture::getVoitureParImmatriculation($immatriculation);
        self::afficherVue($chemin, ["voiture" => $voiture]);
    }

    public static function afficherFormulaireCreation() {
        $chemin = 'voiture/formulaireCreation.php';
        self::afficherVue($chemin, []);
    }
    public static function creerDepuisFormulaire() {
        $imm = $_GET['Immatriculation'];
        $marque = $_GET['Marque'];
        $couleur = $_GET['Color'];
        $nbSieges = $_GET['nbSieges'];
        $voiure = new ModeleVoiture($imm, $marque, $couleur, $nbSieges);
        $voiure->save();
        self::afficherListe();
    }

    private static function afficherVue(string $cheminVue, array $parametres = [])
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }
}
?>