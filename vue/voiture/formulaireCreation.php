<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Car Form</title>
</head>
<body>

<form method="get" action="routeur.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
            <label for="immat_id">Immatriculation</label> :
            <input type="text" placeholder="256AB34" name="Immatriculation" id="immat_id" required/>
        </p>
        <p>
            <label for="marque_id">Marque</label> :
            <input type="text" placeholder="Nissan GTR-34" name="Marque" id="marque_id" required/>
        </p>
        <p>
            <label for="color_id">Couleur</label> :
            <input type="text" placeholder="Black" name="Color" id="color_id" required/>
        </p>
        <p>
            <label for="nbsieges_id">nbSièges</label> :
            <input type="text" placeholder="4" name="nbSieges" id="nbsieges_id" required/>
        </p>
        <p>
            <input type="submit" value="Envoyer" />
            <input type="hidden" name="action" value="creerDepuisFormulaire">
        </p>
    </fieldset>
</form>
</body>
</html>