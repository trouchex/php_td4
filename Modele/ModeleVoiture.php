<?php

require_once "ConnexionBaseDeDonnee.php";
require_once "ModeleVoiture.php";

class ModeleVoiture {

    private $immatriculation;
    private $marque;
    private $couleur;
    private $nbSieges; // Nombre de places assises

    // un getter
    public function getMarque() {
        return $this->marque;
    }

    // un setter
    public function setMarque($marque) {
        $this->marque = $marque;
    }

    public function setCouleur($couleur) {
        $this->couleur = $couleur; // Correction ici
    }

// ...

// un setter
    public function setImmatriculation($immatricualtion) {
        $this->immatriculation = $immatricualtion; // Correction ici
    }

// ...

// un setter
    public function setNbSieges($nbSieges) {
        $this->nbSieges = $nbSieges; // Correction ici
    }

    public function getCouleur() {
        return $this->couleur;
    }

    // un setter


    public function getImmatriculation() {
        return $this->immatriculation;
    }

    // un setter


    public function getnbSieges() {
        return $this->nbSieges;
    }

    // un setter


    // un constructeur
    public function __construct(
        $immatriculation,
        $marque,
        $couleur,
        $nbSieges
    ) {
        $this->immatriculation = $immatriculation;
        $this->marque = $marque;
        $this->couleur = $couleur;
        $this->nbSieges = $nbSieges;
    }

    public static function construireDepuisTableau(array $voitureFormatTableau) : ModeleVoiture
    {
        return new ModeleVoiture($voitureFormatTableau[0],
            $voitureFormatTableau[1],
            $voitureFormatTableau[2],
            $voitureFormatTableau[3]);
    }

    public static function getVoitures() {
        $tableau=array();
        $pdoStatement=ConnexionBaseDeDonnee::getPdo()->query("SELECT * FROM voiture");
        foreach($pdoStatement as $voitureFormatTableau){
            $tableau[]=self::construireDepuisTableau($voitureFormatTableau);
        }
        return $tableau;
    }

    public function save() : void {
        $immatriculation = $this->getImmatriculation();
        $marque = $this->getMarque();
        $couleur = $this->getCouleur();
        $nbSieges = $this->getnbSieges();

        $sql = "INSERT INTO voiture (ImmatriculationBDD, MarqueBDD, CouleurBDD, nbSiegesBDD) 
            VALUES (:immatriculationTag, :marqueTag, :couleurTag, :nbSiegesTag)";

        $pdoStatement = ConnexionBaseDeDonnee::getPdo()->prepare($sql);

        $values = array(
            "immatriculationTag" => $immatriculation,
            "marqueTag" => $marque,
            "couleurTag" => $couleur,
            "nbSiegesTag" => $nbSieges,
            //nomdutag => valeur, ...
        );

        $pdoStatement->execute($values);
}

    public static  function getVoitureParImmatriculation(string $immatriculation) : ?ModeleVoiture
    {
        $sql = "SELECT * from voiture WHERE immatriculationBDD = :immatriculationTag";

        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnee::getPdo()->prepare($sql);




        $values = array(
                "immatriculationTag" => $immatriculation,
                //nomdutag => valeur, ...
            );
            // On donne les valeurs et on exécute la requête
            $pdoStatement->execute($values);



            // On récupère les résultats comme précédemment
            // Note: fetch() renvoie false si pas de voiture correspondante
            $voitureFormatTableau = $pdoStatement->fetch();

            if ($voitureFormatTableau === false) {
                return null;
            }

        return ModeleVoiture::construireDepuisTableau($voitureFormatTableau);
    }




    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString() {
        // À compléter dans le prochain exercice
        return "Marque : $this->marque<br />
            Immatriculation : $this->immatriculation<br />
            Couleur : $this->couleur<br />
            NbSièges :$this->nbSieges<br/>
            ";
    }
}
?>