<?php


require_once '../Configuration/Configuration.php';
class ConnexionBaseDeDonnee {

    private static ?ConnexionBaseDeDonnee $instance = null;
    private PDO $pdo;

    /** construct PDO */

    public function __construct()
    {
        $hostname = Configuration::getHostname();
        $port = Configuration::getHostname();
        $databaseName = Configuration::getDataBase();
        $login = Configuration::getLogin();
        $password = Configuration::getPassword();

        $this->pdo = new PDO("mysql:host=$hostname;port=$port;dbname=$databaseName", $login, $password,
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

// On active le mode d'affichage des erreurs, et le lancement d'exception en cas d'erreur
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    private static function getInstance() : ConnexionBaseDeDonnee {
        // L'attribut statique $pdo s'obtient avec la syntaxe ConnexionBaseDeDonnee::$pdo
        // au lieu de $this->pdo pour un attribut non statique
        if (is_null(ConnexionBaseDeDonnee::$instance))
            // Appel du constructeur
            ConnexionBaseDeDonnee::$instance = new ConnexionBaseDeDonnee();
        return ConnexionBaseDeDonnee::$instance;
    }



    /**
     * @return PDO
     */
    public static function getPdo(): PDO {
        return ConnexionBaseDeDonnee::getInstance()->pdo;
    }

}

